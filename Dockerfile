FROM ubuntu:21.10
EXPOSE 9987/udp 30033/tcp
RUN apt-get update
RUN apt-get install -y wget curl bzip2
#RUN useradd -ms /bin/bash teamspeak
RUN adduser --disabled-login --gecos '' teamspeak
USER teamspeak
RUN wget $(curl -s https://www.teamspeak.com/en/downloads/#server | grep -o 'https://files.teamspeak-services.com/releases/server/.*/teamspeak3-server_linux_amd64-.*.tar.bz2' | head -n1) -O /home/teamspeak/latest.tar.bz2
RUN tar -xjf /home/teamspeak/latest.tar.bz2 -C /home/teamspeak
RUN mv /home/teamspeak/teamspeak3-server_linux_amd64/* /home/teamspeak
RUN rm -rf /home/teamspeak/latest.tar.bz2
RUN rm -rf /home/teamspeak/teamspeak3-server_linux_amd64
#USER root
RUN touch /home/teamspeak/.ts3server_license_accepted
RUN ls -l -a /home/teamspeak/
#RUN /home/teamspeak/ts3server_startscript.sh start inifile=ts3server.ini
WORKDIR /home/teamspeak
CMD ./ts3server


